object P3B {
    def main(args : Array[String]) {
        val Array(n, v) = readLine split " " map {_.toInt}
        val a = Array.fill(n) {
            val Array(t, p) = readLine split " " map {_.toInt}
            (t, p)
        }.zipWithIndex
        val a1 = a filter {_._1._1 == 1} sortBy {-_._1._2}
        val s1 = a1.scanLeft(0){_ + _._1._2}
        val a2 = a filter {_._1._1 == 2} sortBy {-_._1._2}
        val s2 = a2.scanLeft(0){_ + _._1._2}
        val vU = v min (a1.size + a2.size * 2)
        val s = for (i <- 0 to vU / 2) yield {
            s1(vU - i * 2 min a1.size) + s2(i min a2.size)
        }
        val m = s.max
        println(m)
        val k = s indexOf m
        val w = ((a1 take vU - k * 2) ++ (a2 take k)) map {_._2 + 1}
        println(w mkString " ")
    }
}
