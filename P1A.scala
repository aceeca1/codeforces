object P1A {
    def main(args : Array[String]) {
        val Array(n, m, a) = readLine split " " map {_.toLong}
        println(((n + a - 1) / a) * ((m + a - 1) / a))
    }
}
