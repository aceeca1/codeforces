object P9B {
    import io.StdIn._
    import Math._

    def main(args:Array[String]) {
        val Array(n, vb, vs) = readLine.split(' ').map{_.toInt}
        val a = readLine.split(' ').map{_.toDouble}
        val Array(xu, yu) = readLine.split(' ').map{_.toDouble}
        println(1.to(a.size - 1).minBy{i => {
            val xs = xu - a(i)
            a(i) / vb + sqrt(xs * xs + yu * yu) / vs - i * 1e-6
        }} + 1)
    }
}
