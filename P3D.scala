object P3D {
    def main(args : Array[String]) {
        val s = readLine.toArray
        val q = collection.mutable.PriorityQueue[(Int, Int)]()
        def cost(p : Int, d : Int, c : Long) : Long = {
            if (p == s.size) {
                if (d == 0) c else -1
            } else if (s(p) == '(') {
                cost(p + 1, d + 1, c)
            } else {
                val cN = if (s(p) != '?') c else {
                    s(p) = ')'
                    val Array(c1, c2) = readLine split " " map {_.toInt}
                    q enqueue ((c2 - c1, p))
                    c + c2
                }
                if (d > 0) cost(p + 1, d - 1, cN) else {
                    if (q.isEmpty) return -1
                    val (cm, m) = q.dequeue
                    s(m) = '('
                    cost(p + 1, d + 1, cN - cm)
                }
            }
        }
        val c = cost(0, 0, 0L)
        println(c)
        if (c != -1) println(s.mkString)
    }
}
