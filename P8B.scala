object P8B {
    import io.StdIn._

    def main(args:Array[String]) {
        val m = collection.mutable.HashMap[(Int, Int), Int]()
        val s = readLine
        var k = 0
        def f(i:Int, x:Int, y:Int) {
            if (!m.contains((x, y))) {
                m((x, y)) = k
                k += 1
            }
            if (i < s.size) s(i) match {
                case 'L' => f(i + 1, x, y - 1)
                case 'R' => f(i + 1, x, y + 1)
                case 'U' => f(i + 1, x - 1, y)
                case 'D' => f(i + 1, x + 1, y)
            }
        }; f(0, 0, 0)
        val e = Array.fill(k){collection.mutable.Buffer[Int]()}
        for (((x, y), i) <- m) {
            for (xyN <- Array((x + 1, y), (x, y + 1))) m.get(xyN) match {
                case Some(j) => {e(i).+=(j); e(j).+=(i)}
                case None => {}
            }
        }
        val d = Array.fill(k){Int.MaxValue}; d(0) = 0
        val q = collection.mutable.Queue[Int](0)
        while (q.nonEmpty) {
            val qH = q.dequeue
            for (i <- e(qH)) if (d(i) > d(qH) + 1) {
                d(i) = d(qH) + 1
                q.enqueue(i)
            }
        }
        println(if (d(k - 1) == s.size) "OK" else "BUG")
    }
}
