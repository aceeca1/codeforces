object P6D {
    def main(args : Array[String]) {
        val Array(n, a, b) = readLine split ' ' map {_.toInt}
        val h = readLine split ' ' map {_.toInt + 1}
        val hm = h.max + 1
        case class Node(v : Int, from : Int)
        val s = Array.fill(n - 2, hm, hm){Node(Int.MaxValue >>> 1, -1)}
        for (i <- 0 until hm) {
            if (i * b >= h(0)) s(0)(0)(i) = Node(i, -1)
        }
        for {
            i <- 1 until n - 2
            k1 <- 0 until hm
            k2 <- 0 until hm
            k3 <- 0 until hm
            if (k1 * b + k2 * a + k3 * b >= h(i))
            x = s(i - 1)(k1)(k2).v + k3
            if (x < s(i)(k2)(k3).v)
        } s(i)(k2)(k3) = Node(x, k1)
        var answer = (Int.MaxValue, -1, -1)
        for {
            k1 <- 0 until hm
            k2 <- 0 until hm
            if (k1 * b + k2 * a >= h(n - 2))
            if (k2 * b >= h(n - 1))
            x = s(n - 3)(k1)(k2).v
            if (x < answer._1)
        } answer = (x, k1, k2)
        println(answer._1)
        def solve(i : Int, k1 : Int, k2 : Int) {
            if (i < 0) return
            val out = ((i + 2) + " ") * k2
            if (i == 0) println(out.trim) else print(out)
            solve(i - 1, s(i)(k1)(k2).from, k1)
        }
        solve(n - 3, answer._2, answer._3)
    }
}
