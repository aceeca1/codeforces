object P6C {
    def main(args : Array[String]) {
        val n = readLine.toInt
        var t = readLine.split(' ').map{_.toInt}.toVector
        var (a, b) = (0, 0)
        while (t.size > 2) (t.head, t.last) match {
            case (p, q) if p < q => {t = t.init.tail :+ (q - p); a = a + 1}
            case (p, q) if p > q => {t = (p - q) +: t.init.tail; b = b + 1}
            case _ => {t = t.init.tail; a = a + 1; b = b + 1}
        }
        a = a + 1
        if (t.size == 2) b = b + 1
        println(a + " " + b)
    }
}
