object P4C {
    def main(args : Array[String]) {
        val n = readLine.toInt
        val d = collection.mutable.Map[String, Int]()
        for (_ <- 1 to n) {
            val s = readLine
            d get s match {
                case Some(x) => {println(s + x); d(s) = x + 1}
                case None => {println("OK"); d(s) = 1}
            }
        }
    }
}
