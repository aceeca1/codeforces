object P6A {
    def main(args : Array[String]) {
        val a = readLine split ' ' map {_.toInt}
        val answer = () match {
            case _ if (a combinations 3) exists {
                case Array(u, v, w) => u + v > w && v + w > u && w + u > v
            } => "TRIANGLE"
            case _ if (a combinations 3) exists {
                case Array(u, v, w) => u + v >= w && v + w >= u && w + u >= v
            } => "SEGMENT"
            case _ => "IMPOSSIBLE"
        }
        println(answer)
    }
}
