object P5A {
    def main(args: Array[String]) {
        val (add, remove, send) = ("\\+.*".r, "-.*".r, ".*:(.*)".r)
        var p = 0
        var answer = 0
        while (true) readLine match {
            case add() => p = p + 1
            case remove() => p = p - 1
            case send(s) => answer = answer + p * s.size
            case _ => {println(answer); return}
        }
    }
}
