object P9A {
    import io.StdIn._

    def main(args:Array[String]) {
        println(readLine.split(' ').map{_.toInt}.max match {
            case 1 => "1/1"
            case 2 => "5/6"
            case 3 => "2/3"
            case 4 => "1/2"
            case 5 => "1/3"
            case 6 => "1/6"
        })
    }
}
