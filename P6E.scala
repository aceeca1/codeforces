object P6E {
    class SegTree(m : Int, k : Int) {
        val s = 1 << Math.getExponent(m.toFloat) + 1
        val a = Array.fill(s + s){0}
        var time = 1
        def op(n : Int) = {
            def f(d : Int)(n : Int) {
                var i = s + n
                while (i > 1) {
                    val c = i ^ 1
                    val p = i >>> 1
                    if ((i & 1) == d) a(c) = time
                    a(p) = a(i) min a(c) max a(p)
                    i = p
                }
            }
            f(1)(n - k max 0)
            f(0)(n)
            val timeP = time
            time = time + 1
            timeP - a(1)
        }
    }

    def main(args : Array[String]) {
        val Array(n, k) = readLine split ' ' map {_.toInt}
        val h = readLine split ' ' map {_.toInt}
        val st = new SegTree(h.max + 1, k)
        val s = for (hi <- h) yield st op hi
        val s1 = s.max
        val r = for {
            i <- 0 until n
            if (s(i) == s1)
        } yield i + 1
        println(s1 + " " + r.size)
        for (i <- r) println(i - s1 + 1 + " " + i)
    }
}
