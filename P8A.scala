object P8A {
    import io.StdIn._

    def main(args:Array[String]) {
        val (s, s1, s2) = (readLine, readLine, readLine)
        val b1 = (s1 + ".*" + s2).r.findFirstMatchIn(s).nonEmpty
        val (s1r, s2r) = (s1.reverse, s2.reverse)
        val b2 = (s2r + ".*" + s1r).r.findFirstMatchIn(s).nonEmpty
        println(() match {
            case _ if b1 && b2 => "both"
            case _ if b1 => "forward"
            case _ if b2 => "backward"
            case _ => "fantasy"
        })
    }
}
