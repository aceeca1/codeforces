object P7A {
    def main(args : Array[String]) {
        val a = Array.fill(8){readLine} filter {_ != "BBBBBBBB"}
        val c = 0 until 8 count {n => a exists {_(n) == 'B'}}
        println(8 - a.size + c)
    }
}
