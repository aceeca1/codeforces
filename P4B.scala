object P4B {
    def main(args : Array[String]) {
        val Array(d, s) = readLine split " " map {_.toInt}
        val a = Array.fill(d){readLine split " " map {_.toInt}}
        var sE = s - a.map{_(0)}.sum
        if (sE < 0 || s > a.map{_(1)}.sum) {
            println("NO")
        } else {
            println("YES")
            val answer = for (i <- a) yield {
                val uE = sE min i(1) - i(0)
                sE = sE - uE
                i(0) + uE
            }
            println(answer mkString " ")
        }
    }
}
