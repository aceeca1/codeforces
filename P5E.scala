object P5E {
    def main(args: Array[String]) {
        val n = readLine.toInt
        val tk = new java.util.StringTokenizer(readLine)
        val a = Array.fill(n){tk.nextToken.toInt}
        var answer = 0L
        for (a <- Array(a, a.reverse)) {
            case class Node(x : Int, i : Int, r : Int)
            val s = new java.util.ArrayDeque[Node]
            for (i <- 0 to n - 1) {
                val x = a(i)
                while (!s.isEmpty && s.getLast.x < x) s.removeLast
                val r = if (!s.isEmpty && s.getLast.x == x) s.getLast.r else 0
                s addLast Node(x, i, r + 1)
            }
            for (i <- 0 to n - 1) {
                while (!s.isEmpty && s.getFirst.i <= i) s.removeFirst
                val x = a(i)
                while (!s.isEmpty && s.getLast.x < x) {
                    s.removeLast
                    answer = answer + 2
                }
                val r = if (!s.isEmpty && s.getLast.x == x) s.getLast.r else 0
                answer = answer + (r min s.size)
                s addLast Node(x, n, r + 1)
            }
        }
        val v = {
            val a1 = a.max
            val z1 = a count {_ == a1}
            if (z1 > 1) z1.toLong * (z1 - 1) >>> 1 else {
                val b = a filter {_ != a1}
                val a2 = b.max
                b count {_ == a2}
            }
        }
        println((answer >>> 1) - v)
    }
}
