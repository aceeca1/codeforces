object P6B {
    def main(args : Array[String]) {
        val Array(nS, mS, cS) = readLine split ' '
        val (n, m, c) = (nS.toInt, mS.toInt, cS.head)
        val a = Array.fill(n){readLine}
        val d = for {
            x <- 0 until n
            y <- 0 until m
            if (a(x)(y) == c)
            (xA, yA) <- Array((x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1))
            if (0 <= xA && xA < n)
            if (0 <= yA && yA < m)
            z = a(xA)(yA)
            if (z != '.' && z != c)
        } yield z
        println(d.distinct.size)
    }
}
