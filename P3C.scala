object P3C {
    def main(args : Array[String]) {
        val s = readLine + readLine + readLine
        val numX = s count {_ == 'X'}
        val num0 = s count {_ == '0'}
        val toX = numX == num0
        val to0 = numX == num0 + 1
        val filled = s forall {_ != '.'}
        val lines = Array(
            Array(0, 1, 2), Array(3, 4, 5), Array(6, 7, 8),
            Array(0, 3, 6), Array(1, 4, 7), Array(2, 5, 8),
            Array(0, 4, 8), Array(2, 4, 6))
        val winX = lines exists {_ forall {s(_) == 'X'}}
        val win0 = lines exists {_ forall {s(_) == '0'}}
        val answer = () match {
            case _ if !toX && !to0 || toX && winX || to0 && win0 => "illegal"
            case _ if winX => "the first player won"
            case _ if win0 => "the second player won"
            case _ if filled => "draw"
            case _ if toX => "first"
            case _ if to0 => "second"
        }
        println(answer)
    }
}
