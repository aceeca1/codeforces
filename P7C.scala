object P7C {
    def eGcd(a : Long, b : Long) : (Long, Long, Long) = () match {
        case _ if a < 0L => eGcd(-a, b) match {case (p, q, d) => (-p, q, d)}
        case _ if b < 0L => eGcd(a, -b) match {case (p, q, d) => (p, -q, d)}
        case _ if a < b  => eGcd(b, a) match {case (p, q, d) => (q, p, d)}
        case _ if b == 0L => (1, 0, a)
        case _ => eGcd(b, a % b) match {
            case (p, q, d) => (q, p - a / b * q, d)
        }
    }

    def main(args : Array[String]) {
        val Array(a, b, c) = readLine split ' ' map {_.toInt}
        val (p, q, d) = eGcd(a, b)
        if (c % d == 0) {
            val m = -c / d
            println(p * m + " " + q * m)
        } else {
            println("-1")
        }
    }
}
