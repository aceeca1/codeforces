object P5C {
    def main(args: Array[String]) {
        val s = readLine
        val n = s.size
        val a = Array.fill(n + n + 1){n}
        var d = n
        val answer = for (i <- 0 until n) yield {
            s(i) match {
                case '(' => {
                    if (a(d) == n) a(d) = i
                    d = d + 1
                }
                case ')' => {
                    a(d) = n
                    d = d - 1
                }
            }
            i - a(d) + 1
        }
        val m = answer.max max 0
        println(m + " " + answer.count{_ == m})
    }
}
