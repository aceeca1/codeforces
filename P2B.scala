object P2B {
    def main(args : Array[String]) {
        val n = readLine.toInt
        val a = Array.fill(n){
            readLine split " " map {_.toInt}
        }
        val answers = for (d <- Array(2, 5)) yield {
            def factors(x : Int, s : Int = 0) : Int = x match {
                case 0 => 1
                case _ if x % d != 0 => s
                case _ => factors(x / d, s + 1)
            }
            val s = Array.ofDim[Int](n, n)
            s(0)(0) = factors(a(0)(0))
            for (i <- 1 until n) s(0)(i) = s(0)(i - 1) + factors(a(0)(i))
            for (i <- 1 until n) s(i)(0) = s(i - 1)(0) + factors(a(i)(0))
            for {
                i <- 1 until n
                j <- 1 until n
            } s(i)(j) = (s(i - 1)(j) min s(i)(j - 1)) + factors(a(i)(j))
            val route = new Iterator[Char]{
                var (x, y) = (n - 1, n - 1)
                def hasNext = (x, y) != (0, 0)
                def next = {
                    val ret = () match {
                        case _ if y == 0 => 'D'
                        case _ if x == 0 => 'R'
                        case _ if s(x - 1)(y) < s(x)(y - 1) => 'D'
                        case _ => 'R'
                    }
                    ret match {
                        case 'D' => x = x - 1
                        case 'R' => y = y - 1
                    }
                    ret
                }
            }.mkString.reverse
            (s(n - 1)(n - 1), route)
        }
        val a0 = a.map{_ indexOf 0}.zipWithIndex find {_._1 != -1} map {
            case (y, x) =>
                (1, "D" * x + "R" * y + "D" * (n - 1 - x) + "R" * (n - 1 - y))
        }
        val answer = (answers ++ a0).minBy{_._1}
        println(answer._1)
        println(answer._2)
    }
}
