object P9C {
    import io.StdIn._

    def main(args:Array[String]) {
        val s = readLine.toArray
        def g(i:Int) {
            if (i == s.size) return
            s(i) = '1'
            g(i + 1)
        }
        def f(i:Int) {
            if (i == s.size) return
            if (s(i) > '1') g(i) else f(i + 1)
        }; f(0)
        println(Integer.parseInt(s.mkString, 2))
    }
}
