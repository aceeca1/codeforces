object P4D {
    class PrefixMax(m : Int) {
        val s = 1 << Math.getExponent(m.toFloat) + 1
        val a = Array.fill(s + s){(0, 0)}
        def add(n : Int, x : (Int, Int)) {
            var i = s + n
            while (i > 0) {
                if (x._1 > a(i)._1) a(i) = x
                i = i >>> 1
            }
        }
        def get(n : Int) = {
            var i = s + n
            var ans = (0, -1)
            while (i > 1) {
                if ((i & 1) == 1) {
                    val x = a(i - 1)
                    if (x._1 > ans._1) ans = x
                }
                i = i >>> 1
            }
            ans
        }
    }

    def main(args: Array[String]) {
        val Array(n, wS, hS) = readLine split " " map {_.toInt}
        val a = for (id <- (1 to n).toIterator) yield {
            val Array(w, h) = readLine split " " map {_.toInt}
            (w, h, id)
        }
        val aF = a.filter{case (w, h, id) => w > wS && h > hS}.toArray
        val awT = aF.map{_._1}.sorted.distinct
        val awR = awT.zipWithIndex.toMap
        val ahT = aF.map{_._2}.sorted.distinct
        val ahR = ahT.zipWithIndex.toMap
        val aL = aF.map{case (w, h, id) => (awR(w), ahR(h), id)}.sorted
        val s = new PrefixMax(ahT.size + 1)
        val q = collection.mutable.ArrayBuffer[(Int, Int, Int)]()
        var qW = -1
        def flushQ(w : Int) {
            for ((h, i, len) <- q) s.add(h, (len, i))
            q.clear
            qW = w
        }
        val fromN = for (i <- 0 until aL.size) yield {
            val (w, h, _) = aL(i)
            if (w != qW) flushQ(w)
            val (len, from) = s.get(h)
            val lenN = len + 1
            q += ((h, i, lenN))
            from
        }
        flushQ(-1)
        val (len, from) = s.get(ahT.size)
        if (len == 0) {
            println("0")
        } else {
            println(len)
            val b = Iterator.iterate(from)(fromN).takeWhile{_ != -1}.toBuffer
            println(b.reverseMap{i => aL(i)._3} mkString " ")
        }
    }
}
