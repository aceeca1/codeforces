object P1B {
    def main(args : Array[String]) {
        val n = readLine.toInt
        for (_ <- 1 to n) {
            val alNum = "([A-Z]+)([0-9]+)".r
            val rc = "R([0-9]+)C([0-9]+)".r
            readLine match {
                case alNum(al, num) => {
                    val c = al map {_ - 'A' + 1} reduce {_ * 26 + _}
                    println("R" + num + "C" + c)
                }
                case rc(r, c) => {
                    val al = new Iterator[Char] {
                        var x = c.toInt
                        def hasNext = x > 0
                        def next = {
                            val xNew = (x - 1) / 26
                            val ret = (x - xNew * 26 - 1 + 'A').toChar
                            x = xNew
                            ret
                        }
                    }.mkString.reverse
                    println(al + r)
                }
            }
        }
    }
}
