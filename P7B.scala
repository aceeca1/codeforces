object P7B {
    def main(args : Array[String]) {
        val Array(t, m) = readLine split ' ' map {_.toInt}
        var b = collection.mutable.Buffer.fill(m){0}
        val curr = Iterator from 1
        val cAlloc = "alloc (.*)".r
        val cErase = "erase (.*)".r
        val cDefrag = "defragment".r
        for (_ <- 1 to t) readLine match {
            case cAlloc(nS) => {
                val n = nS.toInt
                val k = b.scanLeft(0){
                    (p0, x) => if (x == 0) p0 + 1 else 0
                }.indexWhere(_ >= n.toInt) - 1
                if (k >= 0) {
                    val x = curr.next
                    println(x)
                    for (i <- k - n + 1 to k) b(i) = x
                } else println("NULL")
            }
            case cErase(xS) => {
                val x = xS.toInt
                if (x > 0 && (b contains x)) {
                    b = b map {bi => if (bi == x) 0 else bi}
                } else println("ILLEGAL_ERASE_ARGUMENT")
            }
            case cDefrag() => {
                b = b filter {_ != 0}
                while (b.size < m) b += 0
            }
        }
    }
}
