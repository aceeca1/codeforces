object P7E {
    import io.StdIn._

    val m = collection.mutable.Map[String, Int]().withDefaultValue(0)

    /* import util.parsing.combinator._
    object ExpParsers extends JavaTokenParsers {
        val factor = wholeNumber ^^^ 0 | ident ^^ m | paren
        val term = chainl1(factor, ("[*" + "/]").r ^^ {
            case "*" => (a1:Int, a2:Int) => if (a1 < 3 && a2 < 2) 1 else 3
            case "/" => (a1:Int, a2:Int) => if (a1 < 3 && a2 < 1) 1 else 3
        })
        val expr = chainl1(term, "[+-]".r ^^ {
            case "+" => (a1:Int, a2:Int) => if (a1 < 3 && a2 < 3) 2 else 3
            case "-" => (a1:Int, a2:Int) => if (a1 < 3 && a2 < 2) 2 else 3
        })
        val paren:Parser[Int] = "(" ~> expr <~ ")" ^^ {
            case 3 => 3
            case _ => 0
        }
        def apply(s:String) = parseAll(expr, s).get
    } */

    object ExpParsers {
        import util.matching._
        case class StringSlice(a:String, i:Int) extends CharSequence {
            def charAt(n:Int) = a(i + n)
            def length = a.size - i
            def subSequence(n1:Int, n2:Int) = a.substring(i + n1, i + n2)
            def from(n:Int) = StringSlice(a, i + n)
        }
        val re1 = " *([A-Za-z0-9_]+)".r
        val re2 = (" *([/" + "*])").r
        val re3 = " *([+-])".r
        val re4 = " *\\(".r
        val re5 = " *\\)".r
        def parseFactor(a:StringSlice):(StringSlice, Int) = {
            re1.findPrefixMatchOf(a) match {
                case Some(m1) => (a.from(m1.end), m(m1.group(1)))
                case None => parseParen(a)
            }
        }
        def parseTerm(a:StringSlice) = {
            def parse1(a1:StringSlice, r1:Int):(StringSlice, Int) = {
                re2.findPrefixMatchOf(a1) match {
                    case Some(m1) => parseFactor(a1.from(m1.end)) match {
                        case (a2, r2) if m1.group(1) == "*" =>
                            parse1(a2, if (r1 < 2 && r2 < 2) 1 else 3)
                        case (a2, r2) if m1.group(1) == "/" =>
                            parse1(a2, if (r1 < 2 && r2 < 1) 1 else 3)
                    }
                    case None => (a1, r1)
                }
            }
            parseFactor(a) match { case (a1, r1) => parse1(a1, r1) }
        }
        def parseExpr(a:StringSlice) = {
            def parse1(a1:StringSlice, r1:Int):(StringSlice, Int) = {
                re3.findPrefixMatchOf(a1) match {
                    case Some(m1) => parseTerm(a1.from(m1.end)) match {
                        case (a2, r2) if m1.group(1) == "+" =>
                            parse1(a2, if (r1 < 3 && r2 < 3) 2 else 3)
                        case (a2, r2) if m1.group(1) == "-" =>
                            parse1(a2, if (r1 < 3 && r2 < 2) 2 else 3)
                    }
                    case None => (a1, r1)
                }
            }
            parseTerm(a) match { case (a1, r1) => parse1(a1, r1) }
        }
        def parseParen(a:StringSlice) = {
            re4.findPrefixMatchOf(a) match {
                case Some(m1) => parseExpr(a.from(m1.end)) match {
                    case (a1, r1) => re5.findPrefixMatchOf(a1) match {
                        case Some(m2) =>
                            (a1.from(m2.end), if (r1 < 3) 0 else 3)
                    }
                }
            }
        }
        def apply(s:String) = parseExpr(StringSlice(s, 0))._2
    }

    def main(args : Array[String]) {
        val define = " *# *define +(.*?) +(.*)".r
        for (i <- 1 to readInt) {
            val define(name, exp) = readLine
            m(name) = ExpParsers(exp)
        }
        println(if (ExpParsers(readLine) == 3) "Suspicious" else "OK")
    }
}
