object P5B {
    def main(args: Array[String]) {
        val b = io.Source.stdin.getLines.toBuffer
        val w = b.map{_.size}.max
        println("*" * (w + 2))
        var alt = 0
        for (i <- b) {
            val s = w - i.size
            val s0 = s >> 1
            val s1 = s - s0
            def print(sLeft : Int, sRight : Int) {
                println("*" + " " * sLeft + i + " " * sRight + "*")
            }
            alt match {
                case _ if s0 == s1 => print(s0, s0)
                case 0 => {alt = 1; print(s0, s1)}
                case 1 => {alt = 0; print(s1, s0)}
            }
        }
        println("*" * (w + 2))
    }
}
