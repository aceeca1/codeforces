object P3A {
    def main(args : Array[String]) {
        val (p1, p2) = (readLine, readLine)
        val b = new Iterator[String] {
            var x : Int = p1(0)
            var y : Int = p1(1)
            val tx : Int = p2(0)
            val ty : Int = p2(1)
            def hasNext = x != tx || y != ty
            def next = () match {
                case _ if x < tx && y < ty => {x = x + 1; y = y + 1; "RU"}
                case _ if x < tx && y > ty => {x = x + 1; y = y - 1; "RD"}
                case _ if x > tx && y < ty => {x = x - 1; y = y + 1; "LU"}
                case _ if x > tx && y > ty => {x = x - 1; y = y - 1; "LD"}
                case _ if x < tx => {x = x + 1; "R"}
                case _ if x > tx => {x = x - 1; "L"}
                case _ if y < ty => {y = y + 1; "U"}
                case _ if y > ty => {y = y - 1; "D"}
            }
        }.toBuffer
        println(b.size)
        b foreach println
    }
}
