object P7D {
    import io.StdIn._

    def main(args : Array[String]) {
        val s = readLine
        var p = s(0).toLong
        var q = s(0).toLong
        var r = 1L
        val a = Array.ofDim[Int](s.size)
        a(0) = 1
        for (i <- 1 until s.size) {
            p = p * 0x3333333333333333L + s(i)
            r = r * 0x3333333333333333L
            q = q + r * s(i)
            a(i) = if (p == q) a(i - 1 >> 1) + 1 else 0
        }
        println(a.sum)
    }
}
