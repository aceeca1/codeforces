object P1C {
    import Math._

    case class Complex(x : Double, y : Double) {
        val m = hypot(x, y)
        val p = atan2(x, y)
        def -(n : Complex) = Complex(x - n.x, y - n.y)
    }

    def main(args : Array[String]) {
        def readComplex = {
            val Array(x, y) = readLine split " " map {_.toDouble}
            Complex(x, y)
        }
        val (pA, pB, pC) = (readComplex, readComplex, readComplex)
        val (vAB, vBC, vCA) = (pB - pA, pC - pB, pA - pC)
        val aA = (vAB.p - vCA.p + 2 * PI) % PI
        val aB = (vBC.p - vAB.p + 2 * PI) % PI
        val aC = (vCA.p - vBC.p + 2 * PI) % PI
        val d = (vAB.m + vBC.m + vCA.m) / (sin(aA) + sin(aB) + sin(aC))
        val Array(_, a2, a1) = Array(aA, aB, aC).sorted
        val (r1, r2) = (a1 / PI, a2 / PI)
        val n = 3 to 100 minBy {
            n => {
                val a1 = r1 * n
                val b1 = a1 - rint(a1)
                val a2 = r2 * n
                val b2 = a2 - rint(a2)
                b1 * b1 + b2 * b2 + n * 1e-9
            }
        }
        val answer = d * d * sin(PI * 2 / n) * n * 0.125
        println(answer formatted "%.8f")
    }
}
