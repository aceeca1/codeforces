object P4A {
    def main(args : Array[String]) {
        val x = readLine.toInt
        println(if (x >= 4 && x % 2 == 0) "YES" else "NO")
    }
}
