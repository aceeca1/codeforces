object P2C {
    import Math._

    def main(args : Array[String]) {
        val Array(x1, y1, r1) = readLine split " " map {_.toDouble}
        val Array(x2, y2, r2) = readLine split " " map {_.toDouble}
        val Array(x3, y3, r3) = readLine split " " map {_.toDouble}
        val (x12, y12) = (x1 - x2, y1 - y2)
        val (x13, y13) = (x1 - x3, y1 - y3)
        val d12 = x12 * x12 + y12 * y12
        val d13 = x13 * x13 + y13 * y13
        val e = x12 * y13 - x13 * y12
        val (rr1, rr2, rr3) = (r1 * r1, r2 * r2, r3 * r3)
        val (rr21, rr31) = (rr2 - rr1, rr3 - rr1)
        val ry = rr21 * y13 - rr31 * y12
        val rx = x12 * rr31 - x13 * rr21
        val dy = d12 * y13 - d13 * y12
        val dx = x12 * d13 - x13 * d12
        val r = rx * rx + ry * ry
        val d = dx * dx + dy * dy
        val t = dx * rx + dy * ry + e * e * rr1 * 2.0
        val s = if (r == 0.0) {
            Array(d / (t * 2.0))
        } else {
            val delta = t * t - r * d
            if (delta < 0.0) {
                Array[Double]()
            } else {
                val sD = sqrt(delta)
                val s1 = (t + sD) / r
                val s2 = (t - sD) / r
                Array(s1, s2).filter{_ > 1.0}
            }
        }
        def diff(p1 : Double, p2 : Double) = {
            val d = abs(p1 - p2)
            if (d <= PI) d else 2 * PI - d
        }
        val p = for {
            si <- s
            x = x1 + (si * ry - dy) / (e * 2.0)
            y = y1 + (si * rx - dx) / (e * 2.0)
            p1 = atan2(y1 - y, x1 - x)
            p2 = atan2(y2 - y, x2 - x)
            p3 = atan2(y3 - y, x3 - x)
            b = asin(1.0 / si) * 2.0
            if (diff(p1, p2) >= b)
            if (diff(p1, p3) >= b)
            if (diff(p2, p3) >= b)
        } yield (si, x, y)
        if (p.isEmpty) return
        val (_, x, y) = p.minBy{_._1}
        println(x.formatted("%.5f") + " " + y.formatted("%.5f"))
    }
}
