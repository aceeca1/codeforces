object P5D {
    def tAVS(a : Double, v0 : Double, s : Double) = {
        val delta = v0 * v0 + a * s * 2
        (Math.sqrt(delta) - v0) / a
    }

    def tsAVV(a : Double, v0 : Double, v1 : Double) = {
        val t = (v1 - v0) / a
        (t, (v0 + v1) * 0.5 * t)
    }

    def main(args: Array[String]) {
        val Array(a, v) = readLine split " " map {_.toDouble}
        val Array(l, d, w) = readLine split " " map {_.toDouble}
        val u = v min w
        val (t0u, s0u) = tsAVV(a, 0, u)
        val (t0v, s0v) = tsAVV(a, 0, v)
        val (tuv, suv) = tsAVV(a, u, v)
        val (t0vu, s0vu) = (t0v + tuv, s0v + suv)
        val t1 = d match {
            case s if (s < s0u) => tAVS(a, 0, s)
            case s if (s < s0vu) => t0u + tAVS(a * 0.5, u, s - s0u)
            case s => t0vu + (s - s0vu) / v
        }
        val z = u min a * t1
        val (tzv, szv) = tsAVV(a, z, v)
        val t2 = l - d match {
            case s if (s < szv) => tAVS(a, z, s)
            case s => tzv + (s - szv) / v
        }
        println(t1 + t2 formatted "%.5f")
    }
}
