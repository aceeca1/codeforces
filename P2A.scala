object P2A {
    def main(args : Array[String]) {
        val n = readLine.toInt
        val s = Array.fill(n) {
            val Array(s1, s2) = readLine split " "
            (s1, s2.toInt)
        }.zipWithIndex groupBy {
            case ((name, score), idx) => name
        }
        val maxTotal = s.map{_._2.map{_._1._2}.sum}.max
        val winners = s.filter{_._2.map{_._1._2}.sum == maxTotal}
        val winner = winners.minBy{
            _._2.scanLeft(0, 0){
                case ((idxS, scoreS), ((name, score), idx)) =>
                    (idx, scoreS + score)
            }.find{
                case (idx, scoreS) => scoreS >= maxTotal
            }.getOrElse(Int.MaxValue, 0)._1
        }._1
        println(winner)
    }
}
