object P8C {
    import io.StdIn._

    def main(args:Array[String]) {
        val s = readLine.split(' ').map{_.toInt}
        val a = Array.fill(readInt){readLine.split(' ').map{_.toInt}}
        def dis(a1:Array[Int], a2:Array[Int]) = {
            val d0 = a1(0) - a2(0)
            val d1 = a1(1) - a2(1)
            d0 * d0 + d1 * d1
        }
        val d = Array.tabulate(a.size, a.size){case (i1, i2) =>
            dis(s, a(i1)) + dis(a(i1), a(i2)) + dis(a(i2), s)
        }
        val c = Array.ofDim[Int](1 << a.size)
        for (i <- 1 until 1 << a.size) {
            var i1 = 0
            while (((i >> i1) & 1) == 0) i1 += 1
            var v = c(i & ~(1 << i1)) + d(i1)(i1)
            var i2 = i1 + 1
            while (i2 < a.size) {
                if (((i >> i2) & 1) != 0) {
                    val v2 = c(i & ~(1 << i1) & ~(1 << i2)) + d(i1)(i2)
                    if (v2 < v) v = v2
                }; i2 += 1
            }; c(i) = v
        }
        println(c((1 << a.size) - 1))
        def visit(i:Int) {
            if (i == 0) return
            var i1 = 0
            while (((i >> i1) & 1) == 0) i1 += 1
            var v = c(i & ~(1 << i1)) + d(i1)(i1)
            var k = i1
            var i2 = i1 + 1
            while (i2 < a.size) {
                if (((i >> i2) & 1) != 0) {
                    val v2 = c(i & ~(1 << i1) & ~(1 << i2)) + d(i1)(i2)
                    if (v2 < v) {v = v2; k = i2}
                }; i2 += 1
            }
            print(' '); print(i1 + 1)
            if (k != i1) {print(' '); print(k + 1)}
            print(" 0")
            visit(i & ~(1 << i1) & ~(1 << k))
        }; print('0'); visit((1 << a.size) - 1); println
    }
}
